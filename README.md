# YÜKLEME ve KURULUM
* 1- İlk yapmamız gereken root şifresi belirlemek.
- 2- uçbirim açın
* 3- `sudo passwd root` yazarak root şifresini değiştirin.
- 4- [hp-plugin](https://gitlab.com/yahyayildirim/hp_printer/raw/main/hp-plugin?inline=false) dosyasını indirin.
* 5- hp-plugin dosyasını indirdiğiniz yerde (burası önemli) uçbirim açın.
- 6- `bash hp-plugin` yazıp enter yapın.
* 7- root şifresini belirlediniz mi sorusuna `E` yazarak enter yapın.
- 8- ilgili dosyaları sunucundan indirecek ve kurulum için yüklemeye başlayacaktır. soru sorarsa enter yaparak devam edin.
* 9- root/superuser şifresi istediğinde, daha önce belirlediğiniz root şifresini girin ve enter yapın.
- 10- alt alta Done ve Done görürseniz işlem başarılı bir şekidle tamamlanmış demektir.
